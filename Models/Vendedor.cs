using System.ComponentModel.DataAnnotations;

namespace tech_test_payment_api.Models
{
    public class Vendedor
    {
        [Key]
        public int IdVendedor { get; set; }
        [Required(ErrorMessage = "Obrigatório declarar CPF")]
        public string cpf { get; set; }
        [Required(ErrorMessage = "Obrigatório declarar Nome")]
        public string nome { get; set; }
        public string email { get; set; }
        public string telefone { get; set; }

    }
}