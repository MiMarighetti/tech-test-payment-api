using System.ComponentModel.DataAnnotations;

namespace tech_test_payment_api.Models
{
    public class Venda
    {
        [Key]
        public int IdVenda { get; set; } 
        public int IdVendedor { get; set; } 
        public DateTime Data { get; set; }
        [Required(ErrorMessage = "Obrigatório declarar produtos")]
        public string Produtos { get; set; }
        
        [Required(ErrorMessage = "Obrigatório declarar status")]
        public string Status { get; set; }
    }
}

