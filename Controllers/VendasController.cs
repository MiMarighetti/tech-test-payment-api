using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendasController : ControllerBase
    {
        private readonly PagamentosContext _context;

        public VendasController(PagamentosContext context)
        {
            _context = context;
        }
        [HttpPost("Criar_Venda")]
        public IActionResult Create(Venda venda)
        {
            _context.Add(venda);
            _context.SaveChanges();
            return Ok(venda);
        }

        [HttpGet("Consultar_Venda_ID")]    
        public IActionResult ObterPorId(int id)
        {
            var venda = _context.Vendas.Find(id);
            if (venda == null)
                return NotFound();
            return Ok(venda);
        }

        [HttpPut("Atualização_Venda")]
        public IActionResult Atualizar(int id, Venda venda)
        {
            var vendasBanco = _context.Vendas.Find(id);

            if (vendasBanco == null)
                return NotFound();

            vendasBanco.Produtos = venda.Produtos;
            vendasBanco.IdVendedor = venda.IdVendedor;
            vendasBanco.Data = venda.Data;
            vendasBanco.Status = venda.Status;

            _context.Vendas.Update(vendasBanco);
            _context.SaveChanges();

            return Ok(vendasBanco);
        }


    }
}

       